﻿namespace DigitalDoctor.CardStorage.Core.CardManagementDomain.Exceptions;

public class PatientCardNotFoundException : Exception
{
    public PatientCardNotFoundException(string? message) : base(message)
    {
    }
}