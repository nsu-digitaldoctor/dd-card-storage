﻿using DigitalDoctor.CardStorage.Common.Contracts;
using DigitalDoctor.CardStorage.Core.CardManagementDomain.Entities;
using DigitalDoctor.CardStorage.Core.CardManagementDomain.Exceptions;
using DigitalDoctor.CardStorage.Core.Ports;

namespace DigitalDoctor.CardStorage.Core.CardManagementDomain;

public class CardManagementContext
{
    private readonly ICardManagementStorage _cardManagementStorage;
    
    public CardManagementContext(ICardManagementStorage cardManagementStorage)
    {
        _cardManagementStorage = cardManagementStorage;
    }

    public async Task<Guid> CreatePatientCard(string patientPublicKey)
    {
        return await _cardManagementStorage.AddPatientCard(new PatientCard(patientPublicKey));
    }

    public async Task<PatientCard> AddPatientHistory(PatientHistoryModel model)
    {
        var patientCard = await _cardManagementStorage.GetPatientCardPatientPublicKey(model.PatientPublicKey);
        if (patientCard is null)
        {
            throw new PatientCardNotFoundException($"Нет карты пациента с таким ключем {model.PatientPublicKey}");
        }

        patientCard.PatientHistories.Add(new PatientHistory(model));

        return await _cardManagementStorage.UpdatePatientCard(patientCard);
    }

    public async Task<PatientCard> GetPatientCard(PatientHistoryQuery query)
    {
        var patientCard = await _cardManagementStorage.GetPatientCardPatientPublicKey(query.PatientPublicKey);
        if (patientCard is null)
        {
            throw new PatientCardNotFoundException($"Нет карты пациента с таким ключем {query.PatientPublicKey}");
        }

        return patientCard;
    }
}