﻿using JetBrains.Annotations;

namespace DigitalDoctor.CardStorage.Core.CardManagementDomain.Entities;

[UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
public sealed class PatientCard
{
    /// <summary>
    /// Конструктор для EF
    /// </summary>
    private PatientCard()
    {
        
    }

    /// <summary>
    /// init
    /// </summary>
    /// <param name="patientPublicKey"></param>
    public PatientCard(string patientPublicKey)
    {
        PatientPublicKey = Convert.FromBase64String(patientPublicKey);
    }
    
    /// <summary>
    /// Идентификатор карты пациента
    /// </summary>
    public Guid Id { get; private set; }

    /// <summary>
    /// Публичный ключ пациента
    /// </summary>
    public byte[] PatientPublicKey { get; private set; } = null!;

    /// <summary>
    /// Истории пациента
    /// </summary>
    public List<PatientHistory> PatientHistories { get; private set; } = new();
}