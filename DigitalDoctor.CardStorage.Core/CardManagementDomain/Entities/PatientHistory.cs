﻿using System.Security.Cryptography;
using System.Text;
using DigitalDoctor.CardStorage.Common.Contracts;
using JetBrains.Annotations;

namespace DigitalDoctor.CardStorage.Core.CardManagementDomain.Entities;

[UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
public sealed class PatientHistory
{
    /// <summary>
    /// Конструктор для EF
    /// </summary>
    private PatientHistory()
    {
        
    }

    public PatientHistory(PatientHistoryModel model)
    {
        using var rijndael = new RijndaelManaged();
        using var encryptor = rijndael.CreateEncryptor(model.PatientPrivateKey, model.Vector);
        using var msEncrypt = new MemoryStream();
        using var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);
        var plainBytes = Encoding.UTF8.GetBytes(model.HistoryBody);
        csEncrypt.Write(plainBytes, 0, plainBytes.Length);
        csEncrypt.FlushFinalBlock();
        var encryptedBytes = msEncrypt.ToArray();
        EncryptedHistoryBody = encryptedBytes;
        UserPublicKey = model.PatientPublicKey;
    }
    
    /// <summary>
    /// Идентификатор записи
    /// </summary>
    public Guid Id { get; private set; }

    /// <summary>
    /// Идентификатор карты пациента
    /// </summary>
    public Guid PatientCardId { get; private set; }

    /// <summary>
    /// Публичный ключ пользователя
    /// </summary>
    public byte[]? UserPublicKey { get; private set; } 

    /// <summary>
    /// Зашифрованный текст истории
    /// </summary>
    public byte[]? EncryptedHistoryBody { get; private set; } 
}