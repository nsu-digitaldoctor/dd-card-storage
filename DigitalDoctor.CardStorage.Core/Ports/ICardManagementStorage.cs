﻿using DigitalDoctor.CardStorage.Core.CardManagementDomain.Entities;

namespace DigitalDoctor.CardStorage.Core.Ports;

public interface ICardManagementStorage
{
    Task<Guid> AddPatientCard(PatientCard patientCard);
    Task<PatientCard?> GetPatientCardPatientPublicKey(byte[] patientPublicKey);
    Task<PatientCard> UpdatePatientCard(PatientCard card);
}