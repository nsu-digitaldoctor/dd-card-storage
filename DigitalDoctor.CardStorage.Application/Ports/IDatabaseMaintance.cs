﻿using System.Diagnostics;

namespace DigitalDoctor.CardStorage.Application.Ports;

public interface IDatabaseMaintenance
{
    [DebuggerStepThrough, DebuggerStepperBoundary]
    IDatabaseMaintenance AsNoTrackingWithIdentityResolution();
    
    [DebuggerStepThrough, DebuggerStepperBoundary]
    Task BeginTransaction();
    
    [DebuggerStepThrough, DebuggerStepperBoundary]
    Task CommitTransaction();

    [DebuggerStepThrough, DebuggerStepperBoundary]
    Task RollbackTransaction();
}