﻿using System.Diagnostics;
using DigitalDoctor.CardStorage.Application.Ports;
using Microsoft.Extensions.DependencyInjection;

namespace DigitalDoctor.CardStorage.Application;

/// <summary>
/// Содержит логику контекста исполнения кода. Настройка транзакций, базы и прочие служебные штуки
/// </summary>
[DebuggerStepThrough]
public sealed class ApplicationExecutionContext
{
    private readonly IDatabaseMaintenance _databaseStorage;
    private readonly IServiceProvider _serviceProvider;

    public ApplicationExecutionContext(IDatabaseMaintenance databaseStorage, IServiceProvider serviceProvider)
    {
        _databaseStorage = databaseStorage;
        _serviceProvider = serviceProvider;
    }

    public Task<TResult> RunQuery<TResult>(Func<Task<TResult>> func)
    {
        _databaseStorage.AsNoTrackingWithIdentityResolution();

        return func();
    }

    public async Task RunCommand(Func<Task> func)
    {
        await _databaseStorage.BeginTransaction();

        try
        {
            await func();

            await _databaseStorage.CommitTransaction();
        }
        catch 
        {
            await _databaseStorage.RollbackTransaction();
            throw;
        }
    
    }

    public TDomain ConstructDomain<TDomain>() where TDomain : notnull
    {
        return _serviceProvider.GetRequiredService<TDomain>();
    }
}