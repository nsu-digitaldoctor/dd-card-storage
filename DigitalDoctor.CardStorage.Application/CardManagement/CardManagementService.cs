﻿using System.Security.Cryptography;
using DigitalDoctor.CardStorage.Common.Contracts;
using DigitalDoctor.CardStorage.Common.Contracts.States;
using DigitalDoctor.CardStorage.Core.CardManagementDomain;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DigitalDoctor.CardStorage.Application.CardManagement;

/// <summary>
/// Сервис для работы с историями пациентов
/// </summary>
public class CardManagementService
{
    private readonly ApplicationExecutionContext _applicationContext;
    private readonly CardManagementContext _managementContext;

    /// <summary>
    /// init
    /// </summary>
    /// <param name="applicationContext"></param>
    /// <param name="managementContext"></param>
    public CardManagementService(ApplicationExecutionContext applicationContext, CardManagementContext managementContext)
    {
        _applicationContext = applicationContext;
        _managementContext = managementContext;
    }

    public async Task<Guid> CreatePatientCard(string publicPatientKey)
    {
        return await _applicationContext.RunQuery(async () => 
            await _managementContext.CreatePatientCard(publicPatientKey)
        );
    }

    /// <summary>
    /// Добавит новую историю по ключу пациента
    /// </summary>
    public async Task AddNewPatientHistory(PatientHistoryModel model)
    {
        await _applicationContext.RunQuery(async () => 
            await _managementContext.AddPatientHistory(model)
        );
    }

    /// <summary>
    /// Вернет полную историю болезни пациента
    /// </summary>
    public async Task<PatientCardState> GetPatientCard(PatientHistoryQuery query)
    {
        var result = await _applicationContext.RunQuery(async () => 
            await _managementContext.GetPatientCard(query)
        );

        using var rijndael = new RijndaelManaged();
        using var decryptor = rijndael.CreateDecryptor(query.PatientPrivateKey, query.Vector);
        var patientHistories = new List<PatientHistoryState>();
        foreach (var history in result.PatientHistories)
        {
            using var msDecrypt = new MemoryStream(history.EncryptedHistoryBody);
            await using var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);
            using var srDecrypt = new StreamReader(csDecrypt);
            patientHistories.Add(new PatientHistoryState()
            {
                PatientHistory = await srDecrypt.ReadToEndAsync()
            }); 
        }
        
        return new PatientCardState()
        {
            PatientPublicKey = result.PatientPublicKey,
            PatientHistories = patientHistories
        };
    }
}