﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;

namespace DigitalDoctor.CardStorage.Common.Configuration.Yaml;



/// <summary>
/// Добавляет возможность читать настройки из Yaml файлов.
/// </summary>
public static class YamlConfigurationExtensions
{
    /// <summary>
    /// Добавит файл Yaml как источник конфигурации. 
    /// </summary>
    /// <param name="builder"></param>
    /// <param name="path"></param>
    /// <param name="optional"></param>
    /// <param name="environmentVariablesSection">Данная секция будет читаться как переменные среды</param>
    /// <returns></returns>
    public static IConfigurationBuilder AddYamlFile(this IConfigurationBuilder builder, string path, bool optional = false
        , string environmentVariablesSection = "environmentVariables")
    {
        return builder.AddYamlFile(provider: null
            , path: path
            , optional: optional
            , reloadOnChange: true
            , environmentVariablesSection: environmentVariablesSection);
    }

    /// <summary>
    /// Добавит файл Yaml как источник конфигурации. 
    /// </summary>
    /// <param name="builder"></param>
    /// <param name="provider"></param>
    /// <param name="path"></param>
    /// <param name="optional"></param>
    /// <param name="reloadOnChange"></param>
    /// <param name="environmentVariablesSection">Данная секция будет читаться как переменные среды</param>
    /// <returns></returns>
    public static IConfigurationBuilder AddYamlFile(this IConfigurationBuilder builder
        , IFileProvider? provider
        , string path
        , bool optional
        , bool reloadOnChange
        , string environmentVariablesSection = "environmentVariables")
    {
        if (builder == null)
        {
            throw new ArgumentNullException(nameof(builder));
        }
        if (string.IsNullOrEmpty(path))
        {
            throw new ArgumentException("Empty file path not allowed", nameof(path));
        }

        return builder.AddYamlFile(s =>
        {
            s.FileProvider = provider;
            s.Path = path;
            s.Optional = optional;
            s.ReloadOnChange = reloadOnChange;
            s.EnvironmentVariablesSection = environmentVariablesSection;
            s.ResolveFileProvider();
        });
    }

    private static IConfigurationBuilder AddYamlFile(this IConfigurationBuilder builder, Action<YamlConfigurationSource> configureSource)
    {
        return builder.Add(configureSource);
    }
}