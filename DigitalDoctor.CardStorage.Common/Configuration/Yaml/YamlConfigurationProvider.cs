﻿using Microsoft.Extensions.Configuration;
using YamlDotNet.Core;

namespace DigitalDoctor.CardStorage.Common.Configuration.Yaml;

/// <summary>
/// YAML файл 
/// </summary>
public class YamlConfigurationProvider : FileConfigurationProvider
{
    private readonly string _environmentVariablesSection;

    /// <inheritdoc />
    public YamlConfigurationProvider(YamlConfigurationSource source, string environmentVariablesSection) : base(source)
    {
        _environmentVariablesSection = environmentVariablesSection;
    }

    /// <inheritdoc />
    public override void Load(Stream stream)
    {
        var parser = new YamlConfigurationFileParser(_environmentVariablesSection);
        try
        {
            Data = parser.Parse(stream);
        }
        catch (YamlException e)
        {
            throw new FormatException("Неверный формат Yaml", e);
        }
    }
}