﻿using Microsoft.Extensions.Configuration;

namespace DigitalDoctor.CardStorage.Common.Configuration.Yaml;

/// <summary>
/// YAML файл 
/// </summary>
public class YamlConfigurationSource : FileConfigurationSource
{
    /// <summary>
    /// Название секции записи в которой хранятся в формате переменных среды
    /// </summary>
    /// <remarks>
    /// Пример: key_subkey1_subkey2: value
    /// </remarks>
    public string EnvironmentVariablesSection { get; set; } = null!;

    /// <inheritdoc />
    public override IConfigurationProvider Build(IConfigurationBuilder builder)
    {
        EnsureDefaults(builder);

        return new YamlConfigurationProvider(this, EnvironmentVariablesSection);
    }
}