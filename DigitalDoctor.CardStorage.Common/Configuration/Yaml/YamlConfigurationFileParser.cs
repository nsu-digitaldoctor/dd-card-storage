﻿using Microsoft.Extensions.Configuration;
using YamlDotNet.RepresentationModel;

namespace DigitalDoctor.CardStorage.Common.Configuration.Yaml;


internal sealed class YamlConfigurationFileParser
{
    private readonly string _environmentVariablesSectionName;
    private readonly IDictionary<string, string?> _data = new SortedDictionary<string, string?>(StringComparer.OrdinalIgnoreCase);
    private readonly Stack<string> _context = new();
    private string _currentPath = null!;

    public YamlConfigurationFileParser(string environmentVariablesSectionName)
    {
        _environmentVariablesSectionName = environmentVariablesSectionName;
    }

    public IDictionary<string, string?> Parse(Stream input)
    {
        _data.Clear();
        _context.Clear();

        // https://dotnetfiddle.net/rrR2Bb
        var yaml = new YamlStream();
        yaml.Load(new StreamReader(input, detectEncodingFromByteOrderMarks: true));

        if (yaml.Documents.Any())
        {
            var mapping = (YamlMappingNode)yaml.Documents[0].RootNode;

            // The document node is a mapping node
            VisitYamlMappingNode(mapping);
        }

        return _data;
    }

    private void VisitYamlNodePair(KeyValuePair<YamlNode, YamlNode> yamlNodePair)
    {
        var context = ((YamlScalarNode)yamlNodePair.Key).Value!;
        VisitYamlNode(context, yamlNodePair.Value);
    }

    private void VisitYamlNode(string context, YamlNode node)
    {
        switch (node)
        {
            case YamlScalarNode scalarNode:
                VisitYamlScalarNode(context, scalarNode);
                break;
            case YamlMappingNode mappingNode:
                VisitYamlMappingNode(context, mappingNode);
                break;
            case YamlSequenceNode sequenceNode:
                VisitYamlSequenceNode(context, sequenceNode);
                break;
        }
    }

    private void VisitYamlScalarNode(string context, YamlScalarNode yamlValue)
    {
        //a node with a single 1-1 mapping 
        EnterContext(context);
        var currentKey = _currentPath;

        if (!string.IsNullOrWhiteSpace(_environmentVariablesSectionName))
        {
            var environmentPrefix = $"{_environmentVariablesSectionName}:";
            if (currentKey.StartsWith(environmentPrefix))
            {
                currentKey = currentKey.Replace("__", ":")
                    .Replace(environmentPrefix, string.Empty);
            }
        }

        if (_data.ContainsKey(currentKey))
        {
            throw new FormatException($"Нельзя дублировать ключ в настройках [{currentKey}]");
        }

        _data[currentKey] = IsNullValue(yamlValue) ? null : yamlValue.Value;
        ExitContext();
    }

    private void VisitYamlMappingNode(YamlMappingNode node)
    {
        foreach (var yamlNodePair in node.Children)
        {
            VisitYamlNodePair(yamlNodePair);
        }
    }

    private void VisitYamlMappingNode(string context, YamlMappingNode yamlValue)
    {
        //a node with an associated sub-document
        EnterContext(context);

        VisitYamlMappingNode(yamlValue);

        ExitContext();
    }

    private void VisitYamlSequenceNode(string context, YamlSequenceNode yamlValue)
    {
        //a node with an associated list
        EnterContext(context);

        VisitYamlSequenceNode(yamlValue);

        ExitContext();
    }

    private void VisitYamlSequenceNode(YamlSequenceNode node)
    {
        for (int i = 0; i < node.Children.Count; i++)
        {
            VisitYamlNode(i.ToString(), node.Children[i]);
        }
    }

    private void EnterContext(string context)
    {
        _context.Push(context);
        _currentPath = ConfigurationPath.Combine(_context.Reverse());
    }

    private void ExitContext()
    {
        _context.Pop();
        _currentPath = ConfigurationPath.Combine(_context.Reverse());
    }

    private static bool IsNullValue(YamlScalarNode yamlValue)
    {
        return yamlValue.Style == YamlDotNet.Core.ScalarStyle.Plain
               && (
                   yamlValue.Value == "~"
                   || yamlValue.Value == "null"
                   || yamlValue.Value == "Null"
                   || yamlValue.Value == "NULL"
               );
    }
}