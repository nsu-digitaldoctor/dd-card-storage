﻿using JetBrains.Annotations;
using Newtonsoft.Json.Linq;

namespace DigitalDoctor.CardStorage.Common.Contracts.States;

[PublicAPI]
public sealed record PatientHistoryState
{
    public string PatientHistory { get; set; } = null!;
}