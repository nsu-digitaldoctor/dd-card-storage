﻿using JetBrains.Annotations;

namespace DigitalDoctor.CardStorage.Common.Contracts.States;

[PublicAPI]
public sealed record PatientCardState
{
    /// <summary>
    /// Публичный ключ пациента
    /// </summary>
    public byte[] PatientPublicKey { get; set; } = null!;

    /// <summary>
    /// Истории пациента
    /// </summary>
    public List<PatientHistoryState> PatientHistories { get; set; } = new();
}