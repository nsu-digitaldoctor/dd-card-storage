﻿using JetBrains.Annotations;

namespace DigitalDoctor.CardStorage.Common.Contracts;

[PublicAPI]
public sealed record PatientHistoryQuery
{
    /// <summary>
    /// Публичный ключ пациента
    /// </summary>
    public byte[] PatientPublicKey { get; set; } = null!;

    /// <summary>
    /// Приватный ключ пациента 16 байт
    /// </summary>
    public byte[] PatientPrivateKey { get; set; } = null!;

    /// <summary>
    /// Вектор для шифрования 16 байт
    /// </summary>
    public byte[] Vector { get; set; } = null!;
}