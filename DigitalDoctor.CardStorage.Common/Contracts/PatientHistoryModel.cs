﻿using JetBrains.Annotations;

namespace DigitalDoctor.CardStorage.Common.Contracts;

[PublicAPI]
public class PatientHistoryModel
{
    /// <summary>
    /// Публичный ключ пользователя
    /// </summary>
    public byte[] PatientPublicKey { get; set; } = null!;

    /// <summary>
    /// Приватный ключ пользователя
    /// </summary>
    public byte[] PatientPrivateKey { get; set; } = null!;

    /// <summary>
    /// Вектор для шифрования
    /// </summary>
    public byte[] Vector { get; set; } = null!;

    /// <summary>
    /// Текст истории
    /// </summary>
    public string HistoryBody { get; set; } = null!;
}