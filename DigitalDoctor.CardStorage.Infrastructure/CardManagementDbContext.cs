﻿using System.Reflection;
using DigitalDoctor.CardStorage.Core.CardManagementDomain.Entities;
using Microsoft.EntityFrameworkCore;

namespace DigitalDoctor.CardStorage.Infrastructure;

public class CardManagementDbContext : DbContext
{
    private readonly IServiceProvider _serviceProvider;
    
    public CardManagementDbContext(DbContextOptions<CardManagementDbContext> options, IServiceProvider serviceProvider) : base(options)
    {
        _serviceProvider = serviceProvider;
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
    }

    public DbSet<PatientCard> PatientCard { get; set; }
    public DbSet<PatientHistory> PatientHistory { get; set; }
}