﻿using DigitalDoctor.CardStorage.Core.CardManagementDomain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DigitalDoctor.CardStorage.Infrastructure.GameDomain.EntityFramework.Configurations;

public class PatientCardConfiguration : IEntityTypeConfiguration<PatientCard>
{
    public void Configure(EntityTypeBuilder<PatientCard> builder)
    {
        builder
            .ToTable(nameof(PatientCard));

        builder
            .HasKey(x => x.Id);

        builder
            .Property(x => x.Id)
            .ValueGeneratedOnAdd();

        builder
            .HasMany(x => x.PatientHistories)
            .WithOne()
            .HasForeignKey(x => x.PatientCardId);
    }
}