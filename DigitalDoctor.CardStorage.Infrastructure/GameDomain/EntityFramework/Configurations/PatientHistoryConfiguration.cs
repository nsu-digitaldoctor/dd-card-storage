﻿using DigitalDoctor.CardStorage.Core.CardManagementDomain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DigitalDoctor.CardStorage.Infrastructure.GameDomain.EntityFramework.Configurations;

public class PatientHistoryConfiguration : IEntityTypeConfiguration<PatientHistory>
{
    public void Configure(EntityTypeBuilder<PatientHistory> builder)
    {
        builder
            .ToTable(nameof(PatientHistory));

        builder
            .HasKey(x => x.Id);

        builder
            .Property(x => x.Id)
            .ValueGeneratedOnAdd();
    }
}