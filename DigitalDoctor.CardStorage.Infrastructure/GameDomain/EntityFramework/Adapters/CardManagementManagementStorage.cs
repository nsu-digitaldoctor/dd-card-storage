﻿using DigitalDoctor.CardStorage.Application.Ports;
using DigitalDoctor.CardStorage.Core.CardManagementDomain.Entities;
using DigitalDoctor.CardStorage.Core.Ports;
using Microsoft.EntityFrameworkCore;

namespace DigitalDoctor.CardStorage.Infrastructure.GameDomain.EntityFramework.Adapters;

public class CardManagementManagementStorage : ICardManagementStorage, IDatabaseMaintenance
{
    private readonly CardManagementDbContext _dbContext;

    public CardManagementManagementStorage(CardManagementDbContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task<Guid> AddPatientCard(PatientCard card)
    {
        var result = await _dbContext.PatientCard.AddAsync(card);
        await _dbContext.SaveChangesAsync();
        return result.Entity.Id;
    }

    public async Task<PatientCard?> GetPatientCardPatientPublicKey(byte[] patientPublicKey)
    {
        var patientCard = 
            await _dbContext.PatientCard
                .Include(x => x.PatientHistories)
                .FirstOrDefaultAsync(x => x.PatientPublicKey.SequenceEqual(patientPublicKey));
        return patientCard;
    }

    public async Task<PatientCard> UpdatePatientCard(PatientCard card)
    {
        var updatedCard = _dbContext.PatientCard.Update(card);
        await _dbContext.SaveChangesAsync();
        
        return updatedCard.Entity;
    }

    public IDatabaseMaintenance AsNoTrackingWithIdentityResolution()
    {
        _dbContext.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTrackingWithIdentityResolution;
        return this;
    }

    public async Task BeginTransaction()
    {
        await _dbContext.Database.BeginTransactionAsync();
    }

    public async Task CommitTransaction()
    {
        await _dbContext.Database.CommitTransactionAsync();
    }

    public async Task RollbackTransaction()
    {
        await _dbContext.Database.RollbackTransactionAsync();
    }
}