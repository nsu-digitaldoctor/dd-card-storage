﻿using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace DigitalDoctor.CardStorage.Infrastructure;

/// <summary>
/// Фабрика для миграций
/// </summary>
[UsedImplicitly]
public sealed class DbContextDesignTimeFactory : IDesignTimeDbContextFactory<CardManagementDbContext>
{
    private const string DefaultConnectionString = "Server=93.92.223.199;Port=7003;Database=postgres;User Id=postgres;Password=kj1*!opaq";
    public static DbContextOptions<CardManagementDbContext> GetSqlServerOptions(string? connectionString)
    {
        return new DbContextOptionsBuilder<CardManagementDbContext>()
            .UseNpgsql(connectionString ?? DefaultConnectionString, x =>
            {
                x.MigrationsHistoryTable("__EFMigrationsHistory");
            })
            .Options;
    }
    
    public CardManagementDbContext CreateDbContext(string[] args)
    {
        return new CardManagementDbContext(GetSqlServerOptions(null), null!);
    }
}