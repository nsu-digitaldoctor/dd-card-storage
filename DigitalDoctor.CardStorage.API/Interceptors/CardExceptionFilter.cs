﻿using DigitalDoctor.CardStorage.Core.CardManagementDomain.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace DigitalDoctor.CardStorage.API.Interceptors;

/// <summary>
    /// Маппинг исключений в http-коды ответа.
    /// </summary>
    public class SurveyExceptionFilter : IExceptionFilter
    {
        /// <summary>
        /// Маппинг исключений в http-коды ответа.
        /// </summary>
        public void OnException(ExceptionContext context)
        {
            context.ExceptionHandled = true;
            context.Result = context.Exception switch
            {
                // 400

                // 403

                // 404
                PatientCardNotFoundException ex => new ObjectResult(ex) { StatusCode = StatusCodes.Status404NotFound, Value = ex.Message },

                // 409
                
                // 500

                _ => new ObjectResult(context.Exception) { StatusCode = StatusCodes.Status500InternalServerError }
            };
        }
    }