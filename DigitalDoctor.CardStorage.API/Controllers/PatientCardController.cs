﻿using DigitalDoctor.CardStorage.Application.CardManagement;
using DigitalDoctor.CardStorage.Common.Contracts;
using DigitalDoctor.CardStorage.Common.Contracts.States;
using Microsoft.AspNetCore.Mvc;

namespace DigitalDoctor.CardStorage.API.Controllers;

/// <summary>
/// Контроллер историй пациентов
/// </summary>
[ApiController, Route("/api/cards/")]
public class PatientHistoryController
{
    private readonly CardManagementService _cardManagementService;
    
    /// <summary>
    /// init
    /// </summary>
    /// <param name="cardManagementService"></param>
    public PatientHistoryController(CardManagementService cardManagementService)
    {
        _cardManagementService = cardManagementService;
    }

    [HttpPost, Route("add-card/{patientPublicKey}")]
    public async Task<string> CreatePatientCard([FromRoute] string patientPublicKey)
    {
        await _cardManagementService.CreatePatientCard(patientPublicKey);
        return patientPublicKey;
    }
    
    [HttpPost, Route("add-history")]
    public async Task AddNewPatientHistory(PatientHistoryModel model)
    {
        await _cardManagementService.AddNewPatientHistory(model);
    }

    [HttpPost, Route("get-history/")]
    public async Task<PatientCardState> GetPatientHistory(PatientHistoryQuery query)
    {
        return await _cardManagementService.GetPatientCard(query);
    }
}