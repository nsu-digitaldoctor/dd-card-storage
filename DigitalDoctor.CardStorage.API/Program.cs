using DigitalDoctor.CardStorage.API.Interceptors;
using DigitalDoctor.CardStorage.Application;
using DigitalDoctor.CardStorage.Application.CardManagement;
using DigitalDoctor.CardStorage.Application.Ports;
using DigitalDoctor.CardStorage.Common.DependencyInjection;
using DigitalDoctor.CardStorage.Core.CardManagementDomain;
using DigitalDoctor.CardStorage.Core.Ports;
using DigitalDoctor.CardStorage.Infrastructure;
using DigitalDoctor.CardStorage.Infrastructure.GameDomain.EntityFramework.Adapters;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers(o =>
{
    o.Filters.Add<SurveyExceptionFilter>();
});
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddScoped<CardManagementContext>();
builder.Services.AddScoped<CardManagementService>();
builder.Services.AddDbContextFactory<CardManagementDbContext>(
    options =>
        options.UseNpgsql("Server=93.92.223.199;Port=7003;Database=postgres;User Id=postgres;Password=kj1*!opaq;")
);
builder.Services.AddDbContext<CardManagementDbContext>(ServiceLifetime.Scoped);
builder.Services.AddScoped<CardManagementManagementStorage>()
    .As<ICardManagementStorage>()
    .As<IDatabaseMaintenance>();
builder.Services.AddScoped<ApplicationExecutionContext>();

var app = builder.Build();

using (var scope = app.Services.CreateScope())
{
    var isp = scope.ServiceProvider;
    using var context = isp.GetRequiredService<CardManagementDbContext>();
    context.Database.Migrate();
}

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.MapControllers();

app.Run();